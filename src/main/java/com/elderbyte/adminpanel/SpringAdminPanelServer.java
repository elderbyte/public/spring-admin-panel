package com.elderbyte.adminpanel;

import com.elderbyte.warden.spring.EnableWardenSecurity;
import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Entry point of the spring admin panel service
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableAdminServer
@EnableWardenSecurity
public class SpringAdminPanelServer {

    private static final Logger log = LoggerFactory.getLogger(SpringAdminPanelServer.class);


    public static void main(String[] args) throws UnknownHostException {
        ApplicationContext ctx = SpringApplication.run(SpringAdminPanelServer.class, args);

        Environment env = ctx.getEnvironment();
        log.info("Access URLs:\n----------------------------------------------------------\n\t" +
                        "External: \thttp://{}:{}\n----------------------------------------------------------",
                InetAddress.getLocalHost().getHostAddress(),
                env.getProperty("server.port"));
    }

}

