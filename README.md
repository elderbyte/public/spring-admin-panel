# spring-admin-panel
An administration interface to manage spring / jmx based apps. See [codecentric/spring-boot-admin](https://github.com/codecentric/spring-boot-admin)


## Security & Configuration

The admin panel requires access to all application actuator endpoints, especally the `/metrics`.
These endpoints are usually protected by the role `ACTUATOR`.


### Panel with integrated permission (no login)
Therefore, you have to give this admin panel the `ACTUATOR` (role) permission. 
You can do this by creating a Warden user with the said role, and create & set a user-token in this apps configuration:

Set the env variable **ACTUATOR_TOKEN** to a Warden user token from a user which has the `ACTUATOR` role.

### Login into panel with actuator permission

TODO


## Cloud Deployment

### Supported Environment Variables

| Env                   |      Description                          |  Default      |
|-----------------------|:-----------------------------------------:|--------------:|
| PORT                  | Web app / API port                        | 80            |
| EUREKA_URL            | The absolute eureka endpoint url          | (optional)    |
| ACTUATOR_TOKEN        | Access to actuator info                   | (required)    |

